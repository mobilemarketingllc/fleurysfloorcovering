<?php
// Defines
define( 'FL_CHILD_THEME_DIR', get_stylesheet_directory() );
define( 'FL_CHILD_THEME_URL', get_stylesheet_directory_uri() );

remove_action('wp_head', 'wp_generator');
// Classes
require_once 'classes/class-fl-child-theme.php';

// Actions
add_action( 'wp_enqueue_scripts', 'FLChildTheme::enqueue_scripts', 1000 );

add_action( 'wp_enqueue_scripts', function(){
    wp_enqueue_script("slick",get_stylesheet_directory_uri()."/resources/slick/slick.min.js","","",1);
    wp_enqueue_script("cookie",get_stylesheet_directory_uri()."/resources/jquery.cookie.js","","",1);
    wp_enqueue_script("theme-script",get_stylesheet_directory_uri()."/script.js","","",1);
});

// Register menus
function register_my_menus() {
    register_nav_menus(
        array(
            'footer-1' => __( 'Footer Menu 1' ),
            'footer-2' => __( 'Footer Menu 2' ),
            'footer-3' => __( 'Footer Menu 3' ),
            'footer-4' => __( 'Footer Menu 4' ),
            'footer-5' => __( 'Footer Menu 5' ),
            'site-map' => __( 'Site Map' ),
        )
    );
}
add_action( 'init', 'register_my_menus' );
 
// Enable shortcodes in text widgets
add_filter('widget_text','do_shortcode');


//Facet Title Hook
add_filter( 'facetwp_shortcode_html', function( $output, $atts ) {
    if ( isset( $atts['facet'] ) ) {       
        $output= '<div class="facet-wrap"><strong>'.$atts['title'].'</strong>'. $output .'</div>';
    }
    return $output;
}, 10, 2 );

// Move Yoast to bottom
function yoasttobottom()
{
    return 'low';
}

add_filter('wpseo_metabox_prio', 'yoasttobottom');


// $seleinformation = json_decode(get_option('saleinformation'));

// echo '<pre>';
// $sss = $_GET['promo'];
// print_r($seleinformation->$sss->image_onform);
// print_r($_GET);
// //$seleinformation->
// echo '</pre>';

function index_is_featured( $params, $class ) {
    if ( 'roomvo' == $params['facet_name'] ) { // the facet is named "is_featured"
        $value = (int) $params['facet_value'];
        $display_value = ( 0 == $value ) ? 'Roomvo Enabled' : 'Not Roomvo Enabled'; // change the labels if needed
        $params['facet_display_value'] = $display_value;
    }
    return $params;
}
add_filter( 'facetwp_index_row', 'index_is_featured', 10, 2 );